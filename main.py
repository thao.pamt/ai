import cv2
import numpy as np
import uvicorn
from insightface.app import FaceAnalysis
import hnswlib
from fastapi import FastAPI
from pydantic import BaseModel

face_app = FaceAnalysis(name="buffalo_l", providers=['CUDAExecutionProvider', 'CPUExecutionProvider'])
face_app.prepare(ctx_id=0, det_size=(640, 640))
app = FastAPI()


data = np.loadtxt('data/data.txt')
label = []
with open("data/label.txt", "r") as f:
    for line in f:
        label.append(line.rstrip())

p = hnswlib.Index(space='cosine', dim=512)
p.init_index(max_elements=len(data), ef_construction=200, M=16)
p.set_ef(50)
p.add_items(data, np.arange(len(data)))


def get_embedding_image(path):
    img = cv2.imread(path)
    # cv2.imshow("img", img)
    # cv2.waitKey(0)
    faces = face_app.get(img)
    if len(faces) > 0:
        return faces[0].embedding.reshape(1, -1)
    else:
        return None


def search(path):
    embedding = get_embedding_image(path)
    if embedding is not None:
        ids, distances = p.knn_query(embedding, k=1)
        distances = 1 - distances[0][0]
        if distances > 0.3:
            return label[ids[0][0]]
        else:
            return "Khong phai nguoi trong data "
    else:
        return "Khong xac dinh khuon mat"


# BaseModel for FastAPI
class ImagePath(BaseModel):
    path: str


@app.get("/")
def read_root():
    return {"Hello": "World"}


@app.post("/search")
def search_image(image_path: ImagePath):
    return search(image_path.path)


if __name__ == '__main__':
    uvicorn.run(app, host="0.0.0.0", port=8000)
